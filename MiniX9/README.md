# MiniX9
Udarbejdet af Sif, Gustav og Ann-Katrine




[Link til Sifs GitLab](https://gitlab.com/stetisk-programmering/aestetiskprogrammering)




[Link til Gustavs GitLab](https://gitlab.com/gustavfe/aestetiskprogrammering)




## Programmet
[Link til Sourcekode](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX9/miniX9.js)




[Link til json.fil](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX9/feelings.JSON)




[Link til program](https://ann-katrine.gitlab.io/aestetisk-programmering/MiniX9/index.html) 

![](skarm.png)




## Værket: Artificial Sentiment (AS)
Mennesker skaber følelsesmæssige relationer til andre mennesker, fordi det er en del af vores natur. Men hvad hvis computere også kunne føle, hvordan ville det så se ud? Vi har i dette e-lit værk leget med ideen om en legemliggørelse af computeren, i alt hvad det indebærer at være menneske: krop og sind. Når koden eksekveres, vil værket kreere et digt, som udformer sig som en menneskekrop. I menneskesprog kommunikerer computeren dens følelser, meninger og tanker om det at være en computer. I takt med at det skriftlige digt tager form, kører der en elektronisk melodi, som hvis computeren talte til os på sit eget uforståelige sprog. Og det er netop i denne eksekvering, at Artificial Sentiment sætter spørgsmålstegn computerens rolle som redskab, og hvorledes maskinen indgår i en følelsesmæssig relation til brugeren.

## Beskrivelse af programmet
Vores værk består i sin simpelhed af et digt, hvor digtets fortæller er en computer. Digtet kommer frem på skærmen sætning for sætning og digtets form er et menneskes silhuet. Programmet genererer lyde, som skal symbolisere, hvordan det ville lyde hvis computeren rent faktisk talte til os. Sætningerne er delvist inspireret af AI genereret poesi, for på en måde at give computeren en stemme og indflydelse på værket. AI, som står for artificial intelligence, har også inspireret titel på vores værk. Men fordi vi arbejder med den emotionelle intelligens, har vi byttet ordet intelligens ud med ordet “sentiment”, som betyder følelser. Vi har altså arbejdet med en antropomorfistisk tilgang til computeren - for hvad nu hvis maskinen var bygget op af organer, en hjerne, et hjerte og alt andet der fysisk udgør et menneske, er der så egentlig nogen forskel på dens følelser og vores?

## Beskrivelse af koden
Første skridt i programmeringsfasen var at oprette en Javascript Object Notation fil, også kaldet en JSON fil. JSON filen giver os mulighed for at opbevare den data (vores digt), som vi har brug for at kalde på i vores p5.js fil. Vi havde på forhånd udarbejdet de sætninger (dataen) som skulle udgøre digtet. Disse sætninger havde vi indsat i et array i vores json.fil.

```
{
   "feelings": [
       {
           "ifeel": "initiating"
       },
       {
           "ifeel": "emotional proce-"
       },
       {
           "ifeel": "dure. how long will"
       },

```




En JSON fil starter og slutter med `{}`. Når man opretter et array i en JSON fil skal man `[]`, og for hvert objekt i arrayet bruger man `{}` og skal separeres med kommaer. Det er vigtigt at sætte alle værdierne i anførselstegn `” ___ “`. For at digtets form skulle være en krop, blev vi nødt til at dele sætningerne op.


Med inspiration fra det værk, vi kiggede på i klassen om Queer-kode, har vi også valgt at selve vores kode skal tage afsæt i de temaer vi har fremstillet i vores program. Eksempelvis i vores `set-up function` har vi valgt at navngive vores variabler som følgende.


```
function setup(){
    let sizeOf = windowWidth;
    let myHeart = windowHeight;
    createCanvas(sizeOf, myHeart);
    background(43);
    I = sizeOf/it;
    feel = 30;
    something = myHeart/it;
    frameRate(heartbeat);
    restriction = createButton('allow');
    restriction.size(60, 30);
    restriction.position(I-feel, something);
    restriction.mousePressed(autonomy);
    am = myHeart/it;

```
Det har vi gjort for at understrege legemliggørelsen af computeren. Fx har vi leget med tanken om at frameraten er computerens hjertebank. Og at størrelsen på vores canvas er størrelsen af computerens hjerte.

Med funktionen `mouse.pressed`, starter `functionen autonomy()`. Den ser således ud:

~~~~
function autonomy(){
    iAm = 1;
    restriction.remove();
}
~~~~

Her bliver knappen fjernet og iAm bliver lig 1, hvilket er et krav for at ændre vores if-else-statement. 


I `function draw` har vi lavet et `if-else statement` som har til formål, at der ikke sker noget før der bliver trykket på knappen. Knappen (som vi har kaldt restriction) står i setup. Herunder ses vores draw-funktion, med det if-else-statement vi har beskrevet.

~~~~
function draw(){
    if (iAm === slave) {
    }
    else if(iAm === real) {
        background(43);
        heart();
        what.push(because);
        textAlign(CENTER);
        textSize(10);
        textFont(myHandwriting);
        fill(my, favorite, color);
        noStroke();
        for (let thoughts = 0 ; thoughts < 70 ; thoughts++) {
            text(what[thoughts], I, feel);
            feel = 30+10*thoughts;
        }
        let vocal = random(myVoice);
        vocal.play();
        textSize(40)
        textFont(myShout);
        stroke(43);
        strokeWeight(5);
        text (because, I, am);
~~~~

Når `else if- funktionen` går i gang, starter digtet. Her kaldes på vores `heart()-funktion` som vi vil beskrive længere nede. `what.push(because)` sætter den aktuelle sætning ind i et array. Dette array, kaldet `what[]`, tegnes efterfølgende en sætning ad gangen i et `for() loop` under hinanden. En random lyd fra `vocal[]` arrayet med 5 .wav filer bliver afspillet, og den aktuelle sætning, bliver midlertidigt vist i midten af canvasset med stor skrift, da because er en ny sætning i hvert frame.


`Function heart()` henter sætninger ind fra vores JSON fil. I vores `preload()-funktion` har vi kaldt loadingen af vores JSON fil for “ofMY”, og selve arrayet i JSON filen har vi kaldt “feelings”. Derfor kalder man på sætningerne fra JSON filen ved at sige `ofMY.feelings`, hvilket vi har kaldt aspects. Herefter sætter vi en variabel vi kalder `“ofWhat”` til at kalde på arrayets første sætning. “Now” er defineret som 0, hvilket altid er den første del i et array.

 For at køre igennem sætningen har vi lavet et `if-else-statement`, hvor vi under “if” fortæller at hvis “now” er lig med længden af arrayet, er “now” lig med 0. Ellers (else) skal “now” plusses med 1 (now++), så der køres igennem alle sætningerne. 
 
 Herefter har vi et `if-statement` igen der siger, at hvis vi er nået til sætning nummer 70 i arrayet, skal programmet stoppe med at loope. Til sidst definerer vi endnu en variabel - “because”, som skal fungerer som en form for duplikering af den sætning der bliver skrevet, så sætningen der bliver skrevet bliver vist i stor også.


~~~~
function heart() {
    console.log(because);
    let aspects = ofMy.feelings; 
    let ofWhat = aspects[now];
    if(now == aspects.lenght){
        now = 0;
    } else {
        now++;
    }
    if(now == 70){
        noLoop();
    }
    because = ofWhat.ifeel;
}
~~~~

## Vocable code og e-litteratur (refleksioner om miniX9)
Vocable code er interessant fordi det netop undersøger hvordan kode ikke nødvendigvis altid bare skal reduceres til det funktionelle. I æstetisk programmering kigger vi på samme måde ikke kun på koden, og dét der eksekveres, men også hvordan programmering og kode rører ved  vores kultur. Med vores program opstiller vi en hypotetisk verden, hvori mennesket har skabt en avanceret maskine, som ikke har formålet at opnå forhøjet logik og intellektuel arbejdskraft, men at føle og udtrykke følelser. Det kan sammenlignes med formålet bag vocable code, hvor man ikke tænker optimering og logiske sammenhænge ind, som hovedformål med koden, men i stedet vil skabe og udtrykke følelser.




Lyrik er ofte noget vi ser som printet litteratur. Vi synes derfor, at det var spændende at arbejde med lyrik som e-litteratur. Det tydeliggøre den tætte relation mellem printet- og elektronisk litteratur. Men dér hvor vores stykke e-litteratur adskiller sig fra det printede, er fordi at vores litteratur kun kan tilgås når det bliver eksekveret af en korrekt udført kode. Det er interessant fordi vi med digital litteratur kan opnå nye dimensioner der ikke eksisterer på det printede format. Dette kommer eksempelvis til udtryk i form af formen i vores værk. E-litteraturen har givet os muligheden for at lege med mange aspekter af vores digt, som ikke var muligt, hvis det var printet. Herunder tidslighed, animationer, audio og interaktion. På den måde har vi kunnet visualisere vores pointer om, at give computeren et talerør.




Som Geoff Cox og Alex McLean fremstiller, kan programmering være med til at sætte nogle af de ting op, som er smukke ved kode - og endda ved at trække paralleller til sproget. Vi kan kigge på. Det er også derfor vi har valgt, ikke kun at gøre noget ud af eksekveringen af koden - men også selve hvordan koden er skrevet.




> […] the beauty of code lies in its performance, functionality, and execution. (Geoff Cox & Alex McLean, Vocable Code)




#### Referenceliste
Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38.
Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186

//REGLER
//Når rnd er mindre 0.5 vil der tegnes en ellipse med fyld
//Når rnd større end 0.5 men mindre end 1, vil der tegnes en ellipse uden fyld
//Når rnd er større end 1 vil der ikke tegnes noget

let x = 0;
let y = 0;

const minAfstand = 30; 



function setup() {
  // put setup code here

  createCanvas(windowWidth, windowHeight);

  background(255, 255, 240);

  for (let i = 0; i < 100; i++){
    
    noStroke();
    fill(255, 192, 203, 150);
    ellipse(random(height), random(width), 75);

    //hvorfor vil ellipserne ikke fylde hele skærmen

  }
// forloop der laver baggrunden med random linjer

}

function draw() {
  // put drawing code here


const minRandom = random(1.5);
//konstant

if (minRandom < 0.5 ){

  noStroke();
  fill(219, 112, 147);
  ellipse(x, y+minAfstand, 15);

} else

if (minRandom >= 0.5 && minRandom < 1 ) {
  //større end eller lig med 0.5 OG mindre end 1

  stroke(204, 204, 0);
  noFill();
  ellipse(x, y+minAfstand, 30);

} else

if (minRandom >= 1) {
// der sker ingenting, hvis minRandom er større end 1
//den indsætter bare hvad der ligner et mellemrum


}

x +=minAfstand; //x + x + 20;
if (x> width){
  y = y + minAfstand;
  x = 0;

}

}

# MiniX5

[Link til program](https://ann-katrine.gitlab.io/aestetisk-programmering/MiniX5/index.html)

[Link til source kode](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX5/minix5.js)

![](png.png)

## Beskrivelse af program

Mit program består af en generativ kode. Når du klikker ind på programmet bliver der skabt en masse lyserøde gennemsigte cirkler på random placeringer. Hvis du opdaterer programmet vil cirklerne have nye random placeringer. Hvis du lader programmet køre vil der linje efter linje blive skabt random genereret figurer. Enten vil der komme et mellemrum, en pink cirkel eller en grøn cirkel uden farve i midten. Mønsteret der bliver skabt er forskelligt fra gang til gang, så prøv at opdater for at se hvad der sker nu. 

## Beskrivelse af kode

Min kode er lavet på baggrund af følgende regler

**1. Hvis RND < 0.5 tegnes der en udfyld pink cirkel**

**2.  Hvis RND <= 0.5 OG < 1 tegnes der en cirkel uden fyld med grøn kant**

**3 . Hvis RND > 1 tegnes der ingenting**

**4. Når programmet tændes vil der generes random cirkler som baggrund**

Til dette har jeg gjort brug af `for-loop` og `conditional statements`

Jeg brugte `for-loop` til at tegne cirkerlen som får random placering når programmet tændes. Jeg har skrevet følgende under setup funktionen:

```
 for (let i = 0; i < 100; i++){
    
    noStroke();
    fill(255, 192, 203, 150);
    ellipse(random(height), random(width), 75);

  }

  ```

Til at opfylde de regler jeg havde sat op brugte jeg `if statements`. Først definerede jeg to globale variabler x og y samt en konstant jeg kaldte minAfstand

```

let x = 0;
let y = 0;

const minAfstand = 30; 
```

Jeg brugte variablerne og konstanten til nedenstående if statements. Jeg gjorde også brug af konstanten minRandom med syntaksen `random()`. Som gjorde at der ville blive valgt random tal mellem 0-1.5. 
```
const minRandom = random(1.5);
//konstant

if (minRandom < 0.5 ){

  noStroke();
  fill(219, 112, 147);
  ellipse(x, y+minAfstand, 15);
  //indsætter fyldt lyserød ellipse

} else

if (minRandom >= 0.5 && minRandom < 1 ) {

  stroke(204, 204, 0);
  noFill();
  ellipse(x, y+minAfstand, 30);
// indsætter ellipse yden fyld med grøn kant
} else

if (minRandom >= 1) {
// der sker ingenting, hvis minRandom er større end 1


}
```

## Refleksion om generative koder og randomness
Hvis jeg er helt ærlig, så synes jeg det har været lidt svært at konceptualisere randomness og pseudo randomness. Jeg ved, at der er nogle refleksioner at hente her som kan dreje sig om "hvornår er noget egentligt random?" 

Jeg synes teksten 10_PRINT om randomness var spændende, fordi det var spændende at læse om oprindelsen af ordet random. Og hvordan det engang blev sat i forlængelse af ord som vold og uro. Men i nutiden er vi begyndt at sætte ordet sammen med videnskab og statestik.


Mit program er vel random, fordi jeg har brugt syntakserne `random`og ladet computeren gøre jobbet i at vælge hvilke tal input der skal komme. Det er derfor mit program skifter hver gang man refresher eller åbner det på ny. Jeg gætter på at der er så mange forskellige outcomes og versioner af mit program, at det vil være forskelligt hver gang vi åbner det. 

_Tilføjet senere:_ Dette emne er netop spændende fordi det rejser nogle spørgsmål om hvordan noget i sandhed er random, og om noget overhovedet kan være random. Jeg har jo lavet mig program, så det ligner at det er random genereret. Og det er det vel også på en måde, men det er jo også mig som har lavet reglerne for programmet. Så det er kun "random" indenfor de parametre jeg har sat op. 



#### Referencer

let spiller;
let fjender = [];

let antal = 50;

function setup() {
  // put setup code here

  createCanvas(windowWidth, windowHeight);


  for (let i = 0; i < antal; i++){
    fjender.push(new Fjender());
  }

  spiller = new Spiller();
}


function draw() {
  // put drawing code here

  background(50);
  flereFjender();

  spiller.show();

  if (keyIsDown(LEFT_ARROW)){
    spiller.moveLeft();
  }

  if (keyIsDown(RIGHT_ARROW)){
    spiller.moveRight();
  }

  if (keyIsDown(UP_ARROW)){
    spiller.moveUp();
  }

  if (keyIsDown(DOWN_ARROW)){
    spiller.moveDown();
  }

  //en timer som tæller sekunder 
  let currentTime = int(millis()/ 1000);
  textSize(24);
  fill(255);
  text("DIN TID : "+ currentTime, 50, 50);
  

  //forklaring af spillets regler
  textSize(15);
  text("Brug piletasterne til at undgå de røde prikker", 50, 100);

  collisionCheck();
 
}

function collisionCheck(){

  rectMode(CENTER);
  
  let distance;

  for (let i = 0; i < fjender.length; i++){  

  distance = dist(spiller.posX + spiller.size/2, spiller.posY + spiller.size/2, fjender[i].posX, fjender[i].posY);

  if(distance < 30){
      noLoop();
      textSize(100);
      text("GAME OVER :(", width/2, height/2)
  }
}
}

  function flereFjender() {
    for (let i = 0; i < fjender.length; i++){
      fjender[i].move();
      fjender[i].show();
    }
  
    }
  

[Link til program](https://ann-katrine.gitlab.io/aestetisk-programmering/MiniX6/index.html)

[Link til spillets sourcekode](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX6/spillet.js)

[Link til class for de røde prikker](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX6/Fjender.js)

[Link til class for spilleren](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX6/Spiller.js)

![](118.png)

# Beskrivelse af programmet

Mit spil består af to objekter: røde cirkler (fjenderne) og en grøn firkant. Den grønne firkant er "spilleren", som styres ved at trykke på tastaturets piletaster. Spillets regler er simple: du skal undgå at blive ramt af de røde cirkler. Oppe i højre hjørne er der en tæller, som viser hvor lang tid spillet har været i gang. Altså hvor lang tid du har "overlevet" uden at blive ramt af de røde prikker. Når de røde prikker rammes stopper spillet og der står "GAME OVER" henover skærmen. Siden skal refreshes inden du kan spille igen.


# Forklaring af koden

Jeg har gjort brug af objekt orienteret programmering, som denne uges miniX jo centerer sig om. Det betyder, at man programmerer med henblik på data og objekter, og ikke funktioner og logikker som vi tidligere har brugt. Jeg har lavet to class-filer og én fil hvor mit spil bliver samlet og konstrueret. Heri kalder jeg på det jeg har programmeret i mine class-filer.

Den første class-fil jeg har lavet har jeg kaldt for "Spiller", da det er spillerens egenskaber, adfærd og metoder der bliver beskrevet heri. De properties jeg har valgt at give er: størrelse, position, form og farve. Method for spilleren er, at den skal kunne bevæge sig op og ned og til siderne. Det har jeg gjort på følgende måde: 

```
class Spiller{

    constructor(){
        this.posX = width/2;
        this.posY = height/2;

moveUp() {
        this.posY -= this.speed;
    }

    moveDown(){
        this.posY += this.speed;
    }


    moveLeft() {
        this.posX -= this.speed;
    }

    moveRight(){
        this.posX += this.speed;
    }

```
Jeg har altså brugt mine x og y variabler til at bestemme hvordan min spiller skal bevæge sig. I min spillet.js fil har jeg så kaldt på disse funktioner. Men brugt if-statements til det:

```
spiller.show();

  if (keyIsDown(LEFT_ARROW)){
    spiller.moveLeft();
  }

  if (keyIsDown(RIGHT_ARROW)){
    spiller.moveRight();
  }

  if (keyIsDown(UP_ARROW)){
    spiller.moveUp();
  }

  if (keyIsDown(DOWN_ARROW)){
    spiller.moveDown();
  }
```

Det sværeste i min kode var at få de røde prikker, altså fjenderne, til at spawne mange på skærmen. Til det har jeg gjort brug af for-loops og et tomt array. Først har jeg lavet et tomt array og defineret et antal. Det har jeg gjort globalt
```
let fjender = [];

let antal = 50;

```

Herefter har jeg i min set-up funktion skrevet et for-loop, som siger at jeg gerne vil pushe nye fjender ud i spillet. 

```
for (let i = 0; i < antal; i++){
    fjender.push(new Fjender());
  }

```
I draw funktionen har jeg kaldt på en funktion jeg selv har lavet, som jeg har kaldt for `flereFjender();`

```
 function flereFjender() {
    for (let i = 0; i < fjender.length; i++){
      fjender[i].move();
      fjender[i].show();

```

Noget som desværre ikke fungerer helt optimalt er hvornår mit spil registrerer at man har ramt de røde prikker og ergo er "død". Det er som om den ikke helt forstår hvornår det faktisk rammer og ikke rammer. Nogle gange registrer den det selvom man ikke er ved en rød prik, eller nogle gange registerer den det ikke. 

Jeg har brugt en funktion som jeg har kaldt for `collisionCheck();`, som vi også har brugt i eksemplet fra tomatspillet. Heri har jeg gjort brug af syntakserne `dist` og min spillers x-y position og fjendernes x-y position. Hvis jeg havde mere tid, ville jeg gerne arbejde videre med at få collisionCheck til at passe 100%, da det påvirker hvordan spillet fungerer.

```
let distance;

distance = dist(spiller.posX + spiller.size/2, spiller.posY + spiller.size/2, fjender[i].posX, fjender[i].posY);

  if(distance < 30){
```

# Objekt orienteret programmering og ontologi

Denne uges emne for miniX er objekt orienteret programmering, men også objekt orienteret ontologi. Ontologi betyder verdenssyn. Jeg havde ikke hørt om dette før denne uge, men jeg synes virkelig at det er en spændende og vigtigt måde at beskue verdenen på. Især fordi det minder om den posthumanistiske tankegang, hvorpå man i OOO anskuer mennesket som et objekt i verdenen og IKKE højere end andre objekter. Det synes jeg er interessant, fordi man i posthumanismen heller ej anskuer mennesket som havende en særstilling i verdenen, men netop at mennensket skal sidestilles med alt andet.

Først vil jeg lige skrive lidt om objekt orienteret programmering (OOP). OOP er når man programmerer ved at bruge objekter og data, og man laver hermed nogle abstraktioner. Et godt eksempel på dette er, når man på sit computer skrivebord har afbilledet en abstraktion af en skraldespand. Det har man gjort så vi har mulighed for at forstå de processer der sker indeni i computeren - selvom computeren jo ikke bogstavelig talt smider ting ud i skraldespanden, bare fordi vi trækker en fil over i vores "skraldespand". Det er også vigtigt her at huske, at der er forskellige leveler af abstraktioner. Dét at jeg koder i p5.js er for eksempel en 'high-level' abstraktion, fordi det er blevet human readable. 

I mit program har jeg som sagt gjort brug af denne måde at programmere på. Jeg har lavet nogle objekter på baggrund af nogle abstraktioner af verdenen igennem min kode. Jeg synes, at det her bliver lidt abstrakt at udtrykke ... men jeg vil prøve at forklare hvilke abstraktioner jeg har gjort mig. (Til dig der giver mig feedback: Skriv endelig hvis jeg har misforstået OOP). Men jeg har valgt at gøre mine fjender røde, fordi rød i dette tilfælde er en abstaktion af "det onde" eller "stop". Fx i lyskryds, på stop-skilte og nødsignaler. Jeg vil gerne lære mere om hvordan Objekt orienteret programmering er relevant ift det spil jeg har lavet. 

Nu til lidt om Objekt orienteret ontologi. Som jeg tidligere har nævnt synes jeg det er interessant, at se menensket som endnu et objekt i verdenen, sidestillet med alle andre objekter. Det er desuden spændende fordi man vil kigge på objekterne og ikke de abstraktionerne der er gjort herudfra. Det resonerer godt med mig. Jeg synes især, at abstraktioner kan være "farlige" at agere på. Eller farlig er nok ikke det rigtige ord at bruge (haha). Men det er vigtigt, synes jeg, at have et kritisk blik på hvad de abstraktioner vi laver (også i computerspil) siger om vores verdenssyn. Og hvordan det repræsenterer de biases og repræsentationer af verdenen vi har. Her er der er en interessant parallel at til vores emne om dataficering og data capture, da abstraktioner jo er en dataficering af nogle dataer. Og det har altså en effekt på samfundet, hvis vi begynder at agere ud fra abstraktioner. Et spændende eksempel på dette vi ghettolisterne. Her ville man i obejkt orienteret ontologi kigge på de enkelte objekter (her menes der de mennesker der bor i de såkaldte "ghettoer"), i stedet for de abstarktioner der bliver trukket ud af objekterne. Men desværre (det er min egen mening hehe) bliver der truffet vigtige politiske beslutninger på baggrund af de her abstraktioner, som ikke er repræsentative for alle de objekter (mennesker) der bor i eksempelvis ghettoerne. 

Et andet godt eksempel på hvornår abstraktioner kan være problematiske er repræsentationen af kvinder i (nogle) computerspil... måske faktisk bare nogle steder i populær-kulturen generelt. 

Nå, det var det jeg lige havde reflekteret over. Jeg synes OOO er meget spændende, men jeg er godt klar over at jeg mangler lidt en stærkere kobling mellem programmeringen og ontologien. Jeg tror det bunder i, at jeg bare gerne ville lave et spil som var simpelt, da jeg synes programmering godt kan være lidt svært. Så her fik jeg ikke tænkt særligt meget over OOP ...

##### Referenceliste
* mangler lige at indsætte mine referencer hehe

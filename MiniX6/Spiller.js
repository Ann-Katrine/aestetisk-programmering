class Spiller{

    constructor(){
        this.posX = width/2;
        this.posY = height/2;
        this.speed = 5;
        this.size = 30;
        
    }



    show(){

        rectMode(CENTER);
        fill(153, 255, 51);
        noStroke();
        rect(this.posX, this.posY, this.size);

    }


    moveUp() {
        this.posY -= this.speed;
    }

    moveDown(){
        this.posY += this.speed;
    }


    moveLeft() {
        this.posX -= this.speed;
    }

    moveRight(){
        this.posX += this.speed;
    }

}
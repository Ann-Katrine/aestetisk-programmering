
class Fjender{

    constructor(){

        this.posX = random(0, width);
        this.posY = random(0, height);
        this.speed = random(0.2, 2);
        this.size = random(30, 50);

    }

    show(){
        fill(200, 50, 0, 200);
        noStroke();
        ellipse(this.posX, this.posY, this.size);
        
    }

    move(){
        this.posX += this.speed

        if(this.posX > width){
          this.posX = 0;
        }

    }

}
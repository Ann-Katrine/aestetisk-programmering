# ReadMe miniX 2

**Link til mit program**

[Klik her for at se mine emojis](https://ann-katrine.gitlab.io/aestetisk-programmering/MiniX2/index.html)

**Link til source koden**

[Klik her for koden](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX2/MiniX2.js)

**Screenshot af mit program**

![](Emojis.png)

## Beskrivelse af koden
Min kode består af to alternative emojis. Til højre ser du en orange firkant med øjne, en smilende mund og en cowboyhat (inspireret af min egen yndlings emoji: :cowboy: ). Denne firkant med øjne, hat og mund udgør en ny slags emoji. Til venstre ser du en grøn trekant med store blanke øjne og en lille overrasket mund. 

I min MiniX2 opgave har jeg valgt at gå den simple vej i forhold til min kode. Det har jeg gjort fordi jeg synes, at det var det politiske aspekt der var mere interessant. Altså samtalen om hvordan man kan tænke på alternativer til de emojis vi kender i dag.

Min kode består egentligt bare af en masse geometriske former, som jeg har sat oven på hinanden. Den orange emoji til højre består eksempelvis af en rektangel, altså funktionen `rect()`. Henholdsvis fire cirkler i forskellige størrelser til hvert øje, lavet med `ellipse()` funktionen. Her var det vigtigt, at jeg kodede i den rigtige rækkefølge, så ellipserne lå ovenpå hianden i den rigtige rækkefølge. Munden er lavet med funktionen `arc()`, hvor jeg har lagt på ovenpå hianden. Den øverste er orange, så man kun kan se det nederste af den sorte. Dette skaber en illusion af at munden bare er en bøjet streg. 

Hatten er lavet på samme måde: ved at placere geometriske former ovenpå hinanden i den rigtige rækkefølge. Min kode for hatten ser ud som følgende:

```
fill(102, 51, 0);
noStroke();
rect(80, 190, 240, 20, 15);
//bunden af hatten

fill(102, 51, 0);
noStroke();
rect(130, 140, 140, 40, 15);
// toppen af hatten

fill(10);
noStroke();
rect(130, 170, 140, 20);
//sort bånd på hatten

fill(255);
noStroke();
ellipse(200, 130, 50);
// hvid ellipse til at lave hul i hatten på

```
Det var vigtigt for mig, at lave mange kommenteringer hver gang jeg havde tilføjet noget nyt på koden. Det gjorde jeg ved at bruge `// efterfulgt af det jeg ville skrive `. Formålet med dette var, at jeg gerne ville skabe et overblik i min kode, så den var nemmere at forstå. Både for mig selv, men også andre der kigger på koden bagefter. 

## Mit valg af emojis
Følgende afsnit vil jeg forklare lidt om, hvilke tanker der har ligget bag mit valg af emojis. 

Jeg tog udgangspunkt i den udtalelse som Google gjorde sig i 2016:
> 	"Emojis are fun, playful expression of feelings. Not representations of humans"

Jeg synes, at det er en relevant pointe. Jeg er klar over, at de emojis vi bruger i dag, er repræsentationer af mennesker. Og fordi de netop er det, mener jeg også at samtalen om repræsentation, inklusion og biaser er vigtig. For man kan ikke komme udenom, selvom man måske gerne vil, at dén måde vi taler med hinanden på (både sproget men også for eksempel brugen af emojis), er en repræsentation af de biaser og normativer vi omgiver os med i samfundet. Det er vigtigt, at være klar over dette og kunne reflektere kritisk herom.

Men netop af denne årsag synes jeg, at Googles udtalelse var en interessant overvejelse. Hvad hvis vi havde valgt, at vores emojis ikke skulle repræsentere mennesker - ville det så være muligt at komme udenom mange af problematikker, der opstår når man taler om at være inkluderende? Derfor har jeg valgt at lave to emojis, som ikke er repræsentationer af mennesker. De er inspireret af geometriske former: firkanter og trekanter. Farverne på mine emojis er også farver som menneskehuden ikke har. Jeg har valgt en orange og en grøn farve. Andre emojis kunne eventuelt være blå eller røde. Bevidst har jeg også valgt at gå udenom den klassiske gule farve, for at komme udenom problematikken om at den gulefarve repræsenterer en lys hudfarve. Derudover har emojis jo stadig til formål at formidle nogle følelser og udtryk - det er jo derfor vi bruger dem, har jeg alligvel valgt at tilføje menneskelige inspirerede features: øjne, mund, beklædning osv. 

Jeg ved endnu ikke, om jeg selv synes det er den bedste løsning at gøre alle vores emojis til geometriske ikke-menneskelignende former, for at undgå en stor diskussion om inklusion, normativer, repræsentativitet og biaser. Men jeg synes, at det er interessant at reflektere over hvilke alternativer der er. Og ligesom med så meget andet, så kan man nok ikke komme udenom nogle problematikker alligevel. Min mening er, at vi jo ikke er neutrale objektive mennesker, så alt vi laver har en eller anden form for bias. God eller dårlgi? Men tit kan vi bare ikke selve få øje på den. 

Yes, det var derfor jeg har gjort som jeg har gjort. Tak fordi du læste med. :heart: 

### Referenceliste
[Video af Femke Snelting, hvor Googles udtalelse fremgår](https://www.youtube.com/watch?v=ZP2bQ_4Q7DY&t=2849s&ab_channel=MedeaTV)


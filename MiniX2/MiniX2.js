function setup() {
  // put setup code here
  createCanvas(800, 600);
  background(255);
}

function draw() {
  // put drawing code here

stroke(200, 100, 0);
fill(255, 153,0);
rect(100, 200, 200);

//tilføjet orange firkant

stroke(50);
fill(255);
ellipse(150, 280, 50);
//tilføjet venstre øje

stroke(50);
fill(255);
ellipse(250, 280, 50);
//tilføjet højre øje

noStroke();
fill(102, 0, 102);
ellipse(160, 270, 25);
//tilføjet pupil i venstre øje

noStroke();
fill(10);
ellipse(162, 265, 15);
//tilføjet pupil i venstre øje sort

noStroke();
fill(255);
ellipse(155, 270, 7);
//hvid detalje i øje venstre


noStroke();
fill(102, 0, 102);
ellipse(260, 270, 25);
//pupil i højre øje

noStroke();
fill(10);
ellipse(262, 265, 15);
//sort pupil i høre øje

noStroke();
fill(255);
ellipse(255, 270, 7);
//hvid detajle højre 

fill(10);
arc(200, 315, 70, 80, 0, PI);
//munden

fill(255, 153, 0);
arc(200, 315, 60, 70, 0, PI);
//munden orange ovenpå

fill(102, 51, 0);
noStroke();
rect(80, 190, 240, 20, 15);
//bunden af hatten

fill(102, 51, 0);
noStroke();
rect(130, 140, 140, 40, 15);
// toppen af hatten

fill(10);
noStroke();
rect(130, 170, 140, 20);
//sort bånd på hatten

fill(255);
noStroke();
ellipse(200, 130, 50);
// hvid ellipse til at lave hul i hatten på

fill(51, 102, 0);
stroke(51, 51, 0);
triangle(400, 400, 530, 200, 650, 400);
// TREKANT TILFØJET (alt herunder er på trekanten)

fill(255);
stroke(50);
ellipse(500, 340, 40);
//venstre øje

fill(255);
stroke(50);
ellipse(560, 340, 40);
// højre øje

fill(0);
noStroke();
ellipse(560, 340, 30);
// pupil højre

fill(255);
noStroke();
ellipse(570, 345, 15);
// pupil højre hvid

fill(255);
noStroke();
ellipse(550, 335, 15);
// pupil højre hvid


fill(0);
noStroke();
ellipse(500, 340, 30);
// pupil venstre sort

fill(255);
noStroke();
ellipse(510, 345, 15);
// pupil venstre hvid

fill(255);
noStroke();
ellipse(490, 332, 15);
// pupil venstre hvid

fill(255, 0, 0);
stroke(10);
ellipse(530, 370, 30, 10);
//munden på trekanten

}


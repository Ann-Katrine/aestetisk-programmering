# MiniX11
Udarbejdet af Sif, Gustav og Ann-Katrine




[Link til Sifs GitLab](https://gitlab.com/stetisk-programmering/aestetiskprogrammering)




[Link til Gustavs GitLab](https://gitlab.com/gustavfe/aestetiskprogrammering)




## Programmet
[Link til Sourcekode](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/minix11/minix.js)




[Link til json.fil](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/minix11/minix11.json)




[Link til program](https://ann-katrine.gitlab.io/aestetisk-programmering/minix11/index.html)

![](111.png)

![](12.png)

![](123.png)

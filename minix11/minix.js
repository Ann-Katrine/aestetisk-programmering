let capture;
let myJson;
let ctracker;
let scanner;
let scannerTop;
let scannerBot;

let button;
let button1;
let button2;

let state = 0;
let loading = 0;

let speed = 4;

let a = 255;

function preload(){
    myJson = loadJSON("minix11.json");
    picture1 = loadImage("picture1.png");
    picture2 = loadImage("picture2.png");
    picture3 = loadImage("picture3.png");
}

function setup(){
// Canvas 1:
    createCanvas(windowWidth, windowHeight);
 
    capture = createCapture(VIDEO);
    capture.size(650, 500);
    capture.hide();

    ctracker = new clm.tracker();
    ctracker.init(pModel);
    ctracker.start(capture.elt);

    scanner = windowHeight/2-249;
    scannerTop = windowHeight/2-250;
    scannerBot = windowHeight/2+250;

    button = createButton("Press here to start application process");         //første sides knap
    button.style("backgroundColor","rgba(255, 255, 255, 0.6)");
    button.style("color","rgb(255)");
    //button.style("backgroundOpacity", "0.5");
    button.position(width/2-150, 420);
    button.size(300, 60);
    button.style('font-size', '15px');
    button.mouseOver(skifteFarve);
    button.mouseOut(resetFarve);
    button.mousePressed(trykKnap);

    button1 = createButton("I consent");           //pop-up med samtykke
    button1.style("backgroundColor","#FFFFFF");
    button1.style("color","#FF0000");
    button1.position(950, 540);
    button1.size(160, 50);
    button1.mouseOver(skifteFarve);
    button1.mouseOut(resetFarve);
    button1.mousePressed(trykKnap1);
    button1.hide();

    button2 = createButton("See results");          //knap ved analyse, inden resultater
    button2.style("backgroundColor","rgba(255, 255, 255, 0.6)");
    button2.style("color","#000000");
    button2.style('font-size', '18px');
    button2.position(windowWidth/2+ 175, windowHeight/2 + 275);
    button2.size(150, 65);
    button2.mouseOver(skifteFarve);
    button2.mouseOut(resetFarve);
    button2.mousePressed(trykKnap2);
    button2.hide();

}

function draw(){
////FIRST PAGE////
    if (state == 0){
        background(50, 50, 125);
        image(picture1, 0, 0, width, height + 30);
    }
////POP-UP////
    else if (state == 1){
        fill(255);
        rect(700, 350, 430, 255);
        fill(0);
        textSize(20);
        textStyle(BOLD);
        text("We value your privacy", 730, 390);
        textStyle(NORMAL)
        textSize(15);
        text("In order to run a succesful website, we and certain", 730, 420);
        text("third parties are setting cookies and accessing and", 730, 440);
        text("storing information on your device for various purposes.", 730, 460);
        text("Some third parties are also collecting data for educa-", 730, 480);
        text("tional purposes and require your consent to collect your", 730, 500);
        text("data, to serve you the best experience.", 730, 520);
        button1.show();
    }
////SECOND PAGE////
    else if (state == 2){
        background(0);
        image(picture3, 0, 0, width, height);
        fill(255);
        noStroke();
        rect(width/2-335, height/2-260,  670, 520)
        image(capture, width/2-325, height/2-250,  650, 500);
        positioner = ctracker.getCurrentPosition();

        if(positioner.length){
    
            //laver et for-loop, gennem alle positionerne, beder den om at starte for enden af arrayet, og slutte når den når den sidste position
            for (let i = 0; i < positioner.length; i++) {
                // tegner ellipse for hvert punkt. 
                stroke(50, 50, 50);
                strokeWeight(2);
                noFill();
                ellipse(positioner[i][0]+width/2-325, positioner[i][1]+height/2-250,4);
                //- 325 er halvdelen af billedstørrelsen
            }
        }

        //top to bottom scanner
        strokeWeight(2);
        stroke(0, 204, 0, a);
        line(width/2-325, scanner, width/2+325, scanner);     
            scanner = scanner+speed;
            
        if(scanner > scannerBot || scanner < scannerTop){
            speed = speed*-1;

        }

        //loading bar
        stroke(0);
        strokeWeight(1);
        fill(150);
        rect(width/2-325, height/2+300, 480, 20);
        fill(255);
        rect(width/2-325, height/2+300, loading*4.8, 20);
        fill(0);
        textAlign(CENTER);
        text(floor(loading)+"%", width/2-100, height/2+315);
        if (loading < 100){
            loading = loading+0.05+random(0, 0.4);      //loading speed
        }
        else if (loading >= 100){
            button2.show();           //når den er 100% vises knappen
            a = 0;
            loading = 100;
        }


    } 
////THIRD PAGE////
    else if (state == 3){
        background(0);
        fill(255);
        textAlign(LEFT);
        image(picture2, 0, 0, width, height);
        
        //Tilfældige sætninger
        for (let i = 0 ; i < 5 ; i++){
            sentences();
            textSize(20);
            text(sentence, 220, 280+i*40);
        }

        textSize(20)
        text("Based on the analysis of our artificial interviewer and for the reasons listed above,", 220, 560)
        text("you are not deemed compatible with any job positions at High Tech Companies.", 220, 600) 
        text("Thank you for applying. Further explaintion will not be provided.", 220, 640);
        state = 4;
    }
}


function trykKnap(){
    button.hide();
    state = 1;
}

function trykKnap1(){
    button1.hide();
    state = 2;
}

function trykKnap2(){
    button2.hide();
    state = 3;
}

function skifteFarve(){
    button.style("backgroundColor","rgba(200, 200, 200, 0.6)");
    button1.style("backgroundColor","#E0E0E0");
    button2.style("backgroundColor","rgba(150, 150, 150)");
    //funktion som kaldes på over, for at knappen skifter farve naar musen er paa knappen
}

function resetFarve(){
    button.style("backgroundColor","rgba(255, 255, 255, 0.6)");
    button1.style("backgroundColor","#FFFFFF");
    button2.style("backgroundColor","rgba(255, 255, 255)");
    //funktion som har til formaal at resette farven igen efter musen ikke laengere er inden i knappen
}

function sentences(){
    let loadArray = myJson.analysis;
    let index = int(random(loadArray.length));
    let analysis = loadArray[index].analysis;

    let loadArray2 = myJson.feature;
    let index2 = int(random(loadArray2.length));
    let feature = loadArray2[index2].feature;

    let loadArray3 = myJson.reason;
    let index3 = int(random(loadArray3.length));
    let reason = loadArray3[index3].reason;

    sentence = ["You are " + analysis + " because your " + feature + " " + reason + "."];
}

# MiniX 4

[Link til program](https://ann-katrine.gitlab.io/aestetisk-programmering/MiniX4/index.html)

[Link til sourcekode](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX4/miniX4.js)

![](2.png)

## Forklaring af programmet

Programmet består af et webcam input. Under webcam-billedet er der en stor gul knap hvor der står "_Tryk for en mere skræddersyet oplevevelse**_". Hvis man trykker på knappen kommer der random placerede grønne bokse op med teksten "_REKLAME specialt til dig_". Jo mere du trykker på den gule knap, jo flere reklamer vil der komme op indtil at du er "drunket" i reklamer og ikke længere kan se dig selv. Hvis man er opmærksom opdager man, at der i hjørnet står "_*du siger ja til at sælge alle dine data_". 


## Forklaring af koden

Min kode består af to ting: et webcam input/billede og nogle knap som tilføjer flere knapper. 

Først vil jeg gennemgå hvordan jeg har lavet mit webcam input. 

```
capture = createCapture(VIDEO);
capture.size(600, 480);
capture.hide();

```
Ovenstående kode beskriver hvordan jeg fik webcamet til at virke. Dét der gør at computeren ved at den skal bruge wecam er `capture = createCapture(VIDEO);` Under den linje har jeg indstillet størrelse på billedet til 600 og 480. Først kom der to webcam billeder op, så jeg indsatte syntaksen `capture.hide();` 

Jeg ville udover dette gerne indsætte en knap. Først har jeg defineret nogle globale variabler, som jeg af gode grunde har kaldt for knap. 

```
let knap;

let knap1, knap2, knap3, knap4, knap5, knap6;

```

For at lave den gule knap har jeg skrevet følgende:

```
knap = createButton("Tryk for en mere skræddersyet oplevelse**");
  knap.position(250, 600);
  //første knap
 
  knap.size(200, 100);
  knap.style("background-color", "#FDED00");
  knap.style("color", "#4A235A");
  //knappens styling

  knap.mousePressed(nyknap);
//hvis man trykker på knap vil der ske følgende
```
For at tilføje knappen har jeg brugt syntaksen `createButton();`. Knappens position har jeg defineret til at være 250, 600. Det er vigtigt her at sige, at det er positionen af en absolutte værdi af hele hjemmesiden. Derefter har jeg brugt nogle syntakser til at "style" min knap. Giver den farve, tekst, størrelse osv.

Når jeg trykker på knappen skal der komme en masse nye knapper. Derfor har jeg oprettet en ny funktion som hedder `function nyknap();` Den bruger jeg i syntaksen `knap.mousePressed(nyknap)``

I min nyknap funktion, har jeg lavet et for loop. 

```
function nyknap() {

for (let i = 1; i <=6; i++ ){

  knap[i] = createButton("REKLAME specielt til dig");
  knap[i].position(random(10, 600), random(50, 500));

```
Positionen af de nye knapper skal være random mellem x værdierne (10, 600) og y-værdierne (50, 500));

Det sidste jeg har gjort er at lave et if-statement i draw-funktionen, som siger at hvis musen bliver trykket på vil der komme en tekst op.

```
if (mouseIsPressed === true) {
    textSize(20);
    text("*du siger ja til at sælge alle dine data", 20, 20);

```

## Refleksioner om emnet

Data capture er et meget relevant emne i den digitale tidsalder vi endnu engang lever i. Jeg synes specielt termet "survelliance capitalism" af Shoshana Zuboff var interessant. Det er interessant, men også lidt uhyggeligt, hvordan data om vores adfærd er blevet den nye valuta. Jeg tror, at det for mange er uoverskueligt at tage stilling til hvad det egentligt betyder at færdes på internettet. Og når man så blander det med firmaer,  som pakker deres "gennemsigtighed omkring data" ind i lange kringlede teskter som "data brug og tredjeparter". Så står man pludseligt med et helt samfund hvor den data der findes om os, bliver solgt til højre og venstre. Som Zuboff mener, bliver data om vores færden sin egen valuta som fodrer en kapitalisktiske maskine. I denne uges miniX har jeg taget udgangspunkt i det paradoksale mellem "den gode oplevelse" og den negative konsekvens af at personlig data er blevet en kapitalistisk drivkraft. Firmaer bruger ofte begrundelsen at de gerne vil skabe en mere personlig og skræddersyet oplevelse for deres kunder. Dette gør de via. tilpassede anbefalinger og reklamer. Men jeg tror at vi er mange der glemmer, at for at de kan give os denne her oplevelse, bliver de nødt til at have indsamlet en masse data på os. Vores adfærd på nettet. Og (dette er min mening) firmaerne egentligt mål er jo ikke nødvendigvis at give deres kunder en god oplevelse, for oplevelsen skyld - men for at tjene penge. Der har jeg valgt at man kan klikke på den gule knap for at gøre sin "oplevelse" i mit program bedre. I virkeligheden bliver man bare bombaderet med reklamer, som er specifikt udvalgt til netop den der trykker på knappen. Altså det er bare tænkte reklamer, da der jo står "REKLAME specielt til dig" i mit program. Men det er bare for at demonstrere min vision. Det er også med vilje at den lille tekst i venstre hjørne kun kommer op når man allerede har trykket på knappen. Og teskten er ikke på skærmen længe. Den kan endda være svær at få øje på, da der sker så meget andet på skærmen. Det er et bevidst valg jeg har gjort, som skal symbolisere den måde hvorpå firmaer nogle gange gemmer deres egentlige intentioner væk. Grunden til at jeg har valgt at der også skal være et webcam input/billede, er at det skal demonstrere hvordan vi som individer "drunker" i personlige reklamer og dén genererede data om os, vi måske ikke engang ved eksisterer. Det er altså en meget bogstavelig demonstration af mine refleksioner.

Jeg synes dette emne var meget spændende, og gav mig meget at tænke over. Jeg så dokumentaren om Shoshana Zuboff og lærte en masse jeg ikke havde tænkt på før. Survelliance capitalism er et interesant begreb, og i og med at jeg ikke er vild med den kapitalistiske verden vi lever i, synes jeg det gav endnu mere stof til eftertanke. Jeg tror bare desværre ikke, at jeg kan lade være med at færdes på nettet som jeg gør nu. Og jeg ved heller ikke om jeg behøves? 

#### reference liste
[Dokumentar om survelliance capitalism](https://www.youtube.com/watch?v=hIXhnWUmMvw&list=LL&index=2&ab_channel=vprodocumentary) 


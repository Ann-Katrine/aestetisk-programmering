# MiniX10

Udarbejdet af Sif, Gustav og Ann-Katrine

[Link til Sifs GitLab](https://gitlab.com/stetisk-programmering/aestetiskprogrammering)

[Link til Gustavs GitLab](https://gitlab.com/gustavfe/aestetiskprogrammering)

## Opgavebesvarelse

**Hvilken eksempel kode har I valgt, og hvorfor?**

Vi har valgt eksempelkoden Pix2Pix trænet af Yining Shi. Pix2Pix er en machine learning model, som kan generere et billede input ud fra et billede output. Den er oprindeligt udviklet af Isola et al. Den version som Yining Shi også har taget inspiration fra er Christopher Hesses TensorFlow.js implementation. Som en slags image-to-image, hvor man kan tegne en stregtegning som input. Den vil herefter prøve at generere et billede ud fra din stregtegning og de billeder den er blevet trænet på. 

Yining Shi har lavet sin egen version, som hun har trænet den med 305 billeder af Pikachu og hun har derefter brugt 78 billeder til at teste den. Vi har valgt fordi vi synes det er et interessant koncept. Vi synes det var sjovt at sidde og tegne og afprøve pix2pix. Derfor var vi interesserede i, hvordan det virkede. Det er jo netop et format der giver nem adgang til manipulation mellem input, træningsdata og output. 

![](pix.png)

**Har I ændret noget i koden for at forstå programmet? Hvilke?**

I index.js filen har vi prøvet at indsætte nye værdier i koden. Nederst i index.js filen er der to funktioner som er hhv. blyanten og viskelæderet man tegner med. Funktionerne var meget genkendelige, da der var blevet brugt syntakserne `stroke()` og `strokeWeight()`. Det var nemt at ændre på parametrene og se en forskel.

````
function usePencil() {
  stroke(0);
  strokeWeight(1);
  inputCanvas.removeClass('eraser');
  inputCanvas.addClass('pencil');
}
````

I pix2pix.js klassen har vi også redigeret i nogle parametre, der påvirker hvordan inputtet bliver processeret. Det er talværdierne i `tf.sub` og `tf.div`. De påvirker hvordan stregerne tegnes, og udfyldningen mellem dem. Da baggrunden også er en del af udfyldningen, bliver den også påvirket. Med højere værdier skabes der en del forvrængelse. Det samme sker hvis man skaber ubalance i `tf.mul` og `tf.add`. 

````
function preprocess(inputPreproc) {
      return tf.sub(tf.mul(inputPreproc, tf.scalar(2)), tf.scalar(1));
    }

    function deprocess(inputDeproc) {
      return tf.div(tf.add(inputDeproc, tf.scalar(1)), tf.scalar(2));
    }
````

![](skram.png)




**Hvilke linjer af kode har været særlig interessante for gruppen? Hvorfor?**

Vi har bla. kigget på de `forloops` der er lavet i pix2pix.js filen, og fandt det interessant at finde ud af hvad de var til for. Ordet layer gik igen i begge forloops, men er defineret som et tomt array. Det virker til at dette array bliver brugt til at kombinere flere forskellige funktioner, på en måde så de forskellige funktioner bruges til at undersøge inputtet. Ved at de bliver puttet ind i et array, kan de bruges i forloops til at blive kørt igennem i et ønsket antal gange, så outputtet kan blive forbedret. Dog er disse antagelser på sin vis også baseret på, hvad arrayet og de forskellige funktioner er kaldt, og er et eksempel på hvordan navnene på variablerne enten kan være behjælpelige eller misvisende. Vi kender selvfølgelig forloop syntaksen, men det er svært at være sikker på, hvad hver enkelt linje helt konkret gør. 

````
for (let i = 2; i <= 8; i += 1) {
        const scope = `generator/encoder_${i.toString()}`;
        filter = this.weights[`${scope}/conv2d/kernel`];
        const bias2 = this.weights[`${scope}/conv2d/bias`];
        const layerInput = layers[layers.length - 1];
        const rectified = tf.leakyRelu(layerInput, 0.2);
        convolved = conv2d(rectified, filter, bias2);
        const scale = this.weights[`${scope}/batch_normalization/gamma`];
        const offset = this.weights[`${scope}/batch_normalization/beta`];
        const normalized = batchnorm(convolved, scale, offset);
        layers.push(normalized);
      }
````

Det var interessant for os, at der i koden var en class-fil, og to .js filer. Det har vi jo netop selv arbejdet med da vi havde Objekt Orienteret Programmering. Det kunne vi se, fordi der i pix2pix.js filen i toppen står `class pix2pix {}`. I index.js filen er der oprettet en konstant som  er af typen pix2pix. Den har to parametre: hhv. den træningsfil der har trænet modellen og `model Loaded`. Den type er bygget op ud fra class-filen. 

````
const edges2pikachu = pix2pix('./models/edges2pikachu_AtoB.pict', modelLoaded);

````
**Hvordan ville I udforske/udnytte begrænsningerne af eksempel koden eller machine learning algorithms?**


Det ville være fedt at forsøge at træne pix2pix algoritmen på billeder vi selv vælger. Der er meget fart på AI billede generation lige nu, og pix2pix er et gammelt open source eksempel på dette. Det betyder også, at den gør det en anelse nemmere at forstå hvordan en sådan algoritme bruger machine learning for at skabe grafik. Måden hvorpå output billedet bliver forvrænget på giver også et indblik i, hvordan algoritmen “ser” input tegningen. 

**Var der syntakser/funktioner, som I ikke kendte til før? Hvilke? Og hvad lærte I?**

tf. funktionerne så meget fremmede ud. Det viste sig at referere til en TensorFlow klasse. TensorFlow biblioteket bliver benyttet, som det kan ses i html filen. 
I utils.js ses en funktion, `fetchWeights`, som i første omgang ikke var til at forstå.. “Weights” har med neural networks at gøre, og er de parametre der kan justeres for at give et bestemt output ud fra inputtet. De er på en måde punkter, som inputtet skal igennem, for at blive til det ønskede output.


**Hvordan kan I se en større relation mellem eksempel koden, som I har undersøgt, og brugen af machine learning ude i verdenen (fx. kreative AI, Voice Assistance, selvkørende biler, bots, ansigtsgenkendelse osv.)?**


Som der nævnes som eksempel her, så kan man fx se en relation til selvkørende biler. Det har været svært at forestille sig hvordan selvkørende biler fungerede, men det kan forstås nemmere, hvis man forstår konceptet om machine learning. Efter vi har testet og undersøgt pix2pix koden, er det dog overraskende at finde ud af hvor komplekst et system i en selvkørende bil skal være, for at det er sikkert at lade den køre ude i trafikken. Eksempel koden tog 4 timer at træne, og er langt fra trænet færdig, og det er kun ift. Pikachu.

Det kan også være spændende at overveje hvordan denne form for “tegn -> billede", machine learning, kan være med til at forme arbejdsmåden for forskellige jobs. Man kunne forestille sig, at det vil være relevant at bruge i mange arbejdsfelter. Selvom det nok ville kræve virkelig meget træningsmateriale og data, før at den ville være god at bruge. Og gøre samarbejdet mellem menneske og maskine tættere. Det kunne være en måde, ligesom Voice Assistant er blevet til en samarbejdspartner for mennesket, kunne programmer som pix2pix også bruges som en samarbejdspartner.

**Hvad kunne du tænke dig at vide mere om? Eller hvilke spørgsmål kan du ellers formulere ift. emnet?**


Hvor meget bliver og er machine learning blevet brugt i vores samfund, fx ift arbejdspladser eller underholdning?

Hvordan virker neural networks? Hvis det kan forklares lidt simpelt.

Hvor meget træning/data skal der til for at det bliver godt hver gang man tegner? 







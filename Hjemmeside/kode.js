let myCanvas;
let img;

function preload(){

    img = loadImage('delfin.png');
}

function setup(){
    myCanvas = createCanvas(windowWidth, windowHeight);
    myCanvas.parent("p5Wrapper");
    //background(127);
    textSize(200);

    
}

function draw(){
    push();
    imageMode(CENTER);
    clear();
    image(img, mouseX, mouseY, 300, 300);
    pop();
}

class Fjender{

    constructor(){

        imageMode(CENTER);
        rectMode(CENTER);

        this.posX = random(0, width);
        this.posY = random(0, height);
        this.speed = random(0.2, 2);
        this.size = random(30, 70);

    }

    show(){
        noFill();
        image(fanger, this.posX, this.posY, this.size, this.size);
    }

    move(){
        this.posX += this.speed

        if(this.posX > width){
          this.posX = 0;
        }

    }

}


class Spiller{

    constructor(){
        imageMode(CENTER);
        rectMode(CENTER);
        this.posX = width/2;
        this.posY = height/2;
        this.speed = 5;
        this.size = 50;
        
    }

    show(){
        noFill();
        image(fugl,this.posX, this.posY, this.size, this.size);

    }


    moveUp() {
        this.posY -= this.speed;
    }

    moveDown(){
        this.posY += this.speed;
    }


    moveLeft() {
        this.posX -= this.speed;
    }

    moveRight(){
        this.posX += this.speed;
    }

}
[Link til program](https://ann-katrine.gitlab.io/aestetisk-programmering/MiniX7/index.html)

[Link til spillets sourcekode](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX7/spillet.js)

[Link til class Spiller.js](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX7/Spiller.js)

[Link til class Fjender.js](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX7/Fjender.js)

![](screen1.png)

## Ændringer af miniX

Jeg har valgt at arbejde videre med min MiniX6 projekt i denne uges miniX. Grunden til dette er, at jeg simpelthen ikke var tilfreds til min MiniX6' visuelle udtryk. Jeg ville gerne opgradere og arbejde designiterativt videre med dette program. Jeg synes netop, at det ville være en god måde at vise at jeg havde forstået de forskellige aspekter af ÆP. Og især hvordan det visuelle og æstetiske kommer til udtryk, og hvad dét kan sige om programmet/verdenssyn etc. Først vil jeg gennemgå mine ændringer.

Den første ændring foretog jeg på baggrund af den feedback jeg havde modtaget på min MiniX6. Jeg ændrede mit canvasstørrelse fra `createCanvas(windowWidth, windowHeight);` til følgende: 

 `createCanvas(windowWidth/1.1, windowHeight/1.1)`. Jeg gjorde dette fordi jeg fra min feedback havde lært, at når jeg bare havde mit canvas til browserens vidde og bredde, ville det gøre at browseren registrerer pil-tasterne til at rykke på selve siden og ikke på selve objektet. 
 
 _OBS note til mig selv: tjek lige om dette er rigtigt?_

Herefter begyndte jeg at arbejde med mine class' egenskaber. Hvordan skulle de se ud, spurgte jeg mig selv. Jeg kom frem til at jeg gerne ville gøre 'spilleren' til en sommerfugl og 'fjenderne' til de net man bruger til at fange sommerfugle. Jeg gjorde dette ved at uploade nogle billed filer ved at lave en funktion `function preload(){}` hvor jeg brugte syntaksen `loadImage();`.

```
function preload(){

fanger = loadImage("fanger.png");
fugl = loadImage("fugl.png");
eng = loadImage("eng.jpg");
gameover = loadImage("gameover1.png");
}
```

 Derefter kunne jeg kalde på mine billed-filer i de to js.filer/classes jeg havde. Eksempelvis så det ud som følgende i min Spiller.js fil

```
show(){
    noFill();
    image(fugl,this.posX, this.posY, this.size, this.size);

     }
 ```

Det er første gang jeg prøver at bruge billed-filer i mine miniX projekter. Selvom det måske virker som en simpel opgradering, var det faktisk noget jeg var lidt nervøs for at afprøve. Førhen har jeg bare gjort brug af de figurer og former p5.js kunne lave, fordi jeg gerne ville holde det simpelt og fokusere på at forstå koden. Altså at få mine koder til at fungere inden jeg begyndte at pynte dem og gøre dem flotte. Jeg synes det var på tide, at jeg afprøvede at inddrage eksterne filer i min MiniX projekter.

Jeg har også indsat en billed fil når man taber spillet, så der står `game over` henover skærmen. Det har været et bevidst valg at det skulle være en 'game over' tekst med lidt en retro-stil, fordi mit spil er simpelt og inspireret af simple-retro spil. 


## Relationen mellem ÆP og digital kultur

Æstetisk programmering kan på en måde opdeles i to, som selvfølgelig går hånd i hånd. Nemlig programmeringsaspektet og det æstetiske aspekt. Vi har i dette fag lært at få en grundforståelse for programmering, som ikke er problemløsende, som den der gøres brug af i STEM-fagene. Men en programmerings metode som sætter spørgsmålstegn ved status quo, og gør brug af kritisk refleksion. Det æstetiske aspekt spiller godt indover her. Det er ikke æstetisk, som vi kender det fra "skønheden". Men æstetisk som i noget betydningsdannende - den menneskelige erkendelse, oplevelse, perception. 

Når jeg derfor begrunder mit valg af ændring i min MiniX6 med at jeg gerne ville tilføje et mere æstetisk aspekt, er det altså ikke kun fordi jeg vil gøre mit spil "flottere at se på". Det er fordi jeg gerne vil gøre brug af programmeringen til at sige noget om noget. 

> Continuing the discussion of aesthetics, it should be clear that we do not refer to ideas of beauty as it is commonly misunderstood (aka bourgeois aesthetics), but to political aesthetics: to what presents itself to sense-making experience and bodily perception (Soon & Cox, preface)

Jeg har bevidst valgt at det skulle være en sommerfugl som skulle undgås at blive fanget af en sommerfuglefanger. Og at spillets baggrund skulle være ude i den flotte "natur". Fordi at vi alle har forskellige verdenssyn, vil det jo være forskellige opfattelser der vil ske når man kigger på mit spil. Men mit eget take, er en kritik mod menneskets færden i naturen. I spillet er man den smukke sommerfugl, som skal undgå at blive fanget i sit naturlige habitat. Den bliver fanget af sommerfuglenet, som man kan forstille sig bliver holdt af et menneske.

_NB: note til selv: her er der mere der kan uddybes_

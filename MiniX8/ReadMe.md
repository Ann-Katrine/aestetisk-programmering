# MiniX8 - Flowcharts

[Link til Sifs individuelle del](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/tree/main/minix8?fbclid=IwAR2z81S5FX3bOdDwzLvxhboPyYpf5qSOwf2xaNeQ3Xy-I5_eMZwZBnJDjVE)

[Link til Gustavs individuelle del](https://gitlab.com/gustavfe/aestetiskprogrammering/-/tree/main/miniX8?fbclid=IwAR1SlGLNa8yB5q9yeQxux7leZ_29bqL21lPq8E0b2Tt-Qcqp6RC28P4wUf0)

## Individuel del

Jeg har i min individuelle del af denne miniX, har jeg udarbejdet et flowchart for min MiniX6. Jeg skulle lave et flowchart for dét miniX projekt der var mest komplekst, og netop derfor valgte jeg mit spil. Spillet går kort sagt ud på, at man styrer en grøn firkant via piletasterne, og skal prøve at undgå de røde cirkler som flyver henover skærmen. Hvis den grønne firkant rammer de røde cirkler stopper spillet. 

Mit flowchart ser ud som nedenstående. Jeg har fokuseret på at lave et flowchart som repræsenterer hvordan man spiller spillet, altså fra brugerens perspektiv. Jeg har forsøgt at beskrive spillets fremgang på en simpel måde. 

![](Skarmbillede.png)


## Gruppe del (Sif, Gustav & Ann-Katrine)

### Flowchart 1 - translator

Vores første idé går ud på at give opmærksomhed til den tillid, vi udviser over for digitale redskaber, og diskutere de bias der ligger i deres algoritmer. Selve programmet ville bestå af to textboxe, og en knap, og være opsat i stil med de mest brugte oversættelsesværktøjer der findes online (F.eks. google translate.) I dette tilfælde vil udgangspunktet være oversættelse til det finske sprog, da dets sprogstamme ligger tilpas langt fra vores danske, til at det kræver en del tillid at lade maskinen stå for oversættelsen. Man skriver en tekst ind i textboxen, og trykker derefter på “oversæt” knappen eller ENTER-tasten. Vi genererer på forhånd en liste med ti forskellige sætninger, som kan bruges. Disse skal vi selv bruge oversættelsesværktøjer for at generere. Én tilfældig sætning fra listen udskrives i boxen til højre, og poserer som oversættelse til brugerens input.


![](billede1.png)


### Flowchart 2 - facetracker

Vores anden idé er inspireret af emnet data-capture og capitalism surveillance. Programmet går ud på, at der kommer et webcam input op, som indeholder en facetracker. Så når der vises et ansigt på webcammet vil facetrackeren lægge sig ovenpå ansigtet. Under webcam inputtet er der en knap, som man kan trykke på. Når man trykker på knappen vil en tekst komme op med personlighedstræk dukke op. Det er meningen at disse personlighedstræk skal forestille sig, at være baseret på dét data som facetrackeren har indsamlet om den person der er på webcammet. (Selvfølgelig baseret på nogle data, som allerede er fundet). Dette er et kritisk program, som undersøger hvordan algoritmer bruger og indsamler data om mennesker. Og hvordan algoritmer kan bruge denne data til at kategorisere personer på baggrund af eksempelvis deres udseende. 

![](billede2.png)


## Refleksioner

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**
 

Når man har med noget så detaljeret og kompliceret som et værk man har programmeret, er der mange dele der er mulige at inddrage i sin flowchart. Derfor var det behjælpeligt for os at snakke om hvor detaljeret vores flowchart skulle være, om vi ville holde det simpelt, eller ville have alle små trin med. I forhold til vores flowchart, valgte vi at holde det simpelt ved kun at inddrage de vigtigste trin. Det vil sige at vi ikke har inddraget hvordan programmet skal programmeres eller grundlæggende fungerer, da dette kan blive meget uoverskueligt, men har skrevet hvilke inputs og handlinger der skal være i vores program for at skabe det rette udtryk ud fra vores idé. Ved at vi holder os til kun at inddrage inputs og handlinger i vores flowchart, kan vi dermed holde det på et komplekst niveau, hvor vi er enige om hvilke elementer vi skal programmere.


**What are the technical challenges facing the two ideas and how are you going to address these?**


I værket ‘Translator’ vil en de tekniske udfordringer bestå i at bruge DOM-elementer, og få inputtet til at give et output der er overbevisende nok. Eventuelt ved at lave sammenligninger mellem inputtet og de finske sample sætninger, så længderne stemmer nogenlunde overens. Det kan gøres med en tæller, der øges ved keypress og reduceres ved backspace eller delete, og når tælleren er inden for et specifikt længde-interval, gives en tilsvarende finsk sætning.


I vores idé 2 vil en af vores udfordringer blive, at finde ud af hvordan vi får det til at virke som om ansigtstrackeren har en påvirkning på de personlighedstræk. Vi skal finde ud af hvor mange personlighedstræk der skal være, og hvor mange gange de samme går igen. 


**In which ways are the individual and the group flowcharts you produced useful?**


Vi har taget udgangspunkt i at lave flowcharts ud fra brugerens oplevelse af programmet. Det hjælper med at forstå hvordan programmet kommer til at virke når det er blevet lavet. Selvom vi ikke har valgt at lave flowchart over hvordan det skal programmeres, er det stadig en stor hjælp at forstå hvordan programmet i sin færdighed kommer til at fungere. Altså hvorledes vi skal programmere det, og hvilke syntaks der skal tages i brug. I selve udarbejdelsen af vores flowcharts har vi fået diskuteret og vendt nogle refleksioner om både programmeringsprocessen, men også vores design-valg. Det har været et godt for os at lave flowcharts, og en god måde hvorpå man kan belyse de algoritmiske procedurer af programmet inden det bliver lavet - og ikke mindst skabe en fælles forståelse for vores idéer.. Der kan trækkes paralleller til dét at pseudokode, og hvorfor det faktisk også er en måde hvorpå man kan simplificere og skabe forståelse for ens kode, inden man går i gang med at programmere. 


# MiniX 1 ReadMe

##### Link til mit program
[Klik her](https://ann-katrine.gitlab.io/aestetisk-programmering/MiniX%201/index.html)

##### Screenshot af mit program
![](Screenshot_.png)

##### Link til Source koden
[Klik her](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX%201/minix_1.js)

## Beskrivelse af mit program
Velkommen til mit første MiniX projekt. *Enjoy* :sunglasses: :heart_eyes: :punch:

Mit program består af en række geometriske former. Nogle af formerne er stillestående, mens andre bevæger sig. Der er gjort brug af både 3d og 2d funktioner. 

Jeg tog en kunsterneristisk, måske en smule abstrakt, tilgang til min MiniX 1. Da jeg var kommet i gang, gik det op for mig, at jeg ikke kunne lade være med at tænke på to kunstnere: Hilma af Klint og Richard Mortensen. Jeg har set en udstilling af Richard Mortensen på Trapholt Museum. I denne udstilling gør Mortensen brug af former. Det var netop dét jeg blev ved med at tænke på, da jeg selv begyndte at kode forskellige geometriske former. Det er nogle "simple", men flotte værker Richard Mortensen har udstillet på Trapholt museum. Derudover kunne jeg heller ikke lade være med at tænke på Hilma af Klint. Hun har lavet nogle smukke værker, hvor jeg især er betaget af hendes farvebrug. Den måde hvorpå farverne spiller sammen, og skaber et harmoisk farveunivers, synes jeg er enormt inspirerende. Jeg brugte selv lang tid på at vælge hvilke farver jeg skulle bruge til de former jeg havde kodet.



## Beskrivelse af koden
Følgende afsnit vil jeg beskrive hvordan jeg har kodet mit program. Jeg vil ikke forklare alle de koder jeg har skrevet, men sætte fokus på dem jeg netop synes er særligt spændende.

Det jeg brugte længst tid på at lave var 3d boksen. Jeg har gjort følgende:

```
function setup() {
  // put setup code here
  createCanvas(500, 500, WEBGL);
  //canvas, men ved WEBGL for at det kan være 3d

```

Da jeg har gjort brug af 3d, har jeg i mit kode setup skrevet "WEBGL". Dette skrives når man opretter et canvas med createCanvas- funktionen. WEBGL gør således at der oprettes en z-dimension, så det er muligt at lave 3d. Så der vil være x, y, z - akser i koordinatsystemet.

```
push();
noFill();
stroke(153, 0, 51);
background(204, 153, 255);
rotateX(frameCount * 0.01);
rotateY(frameCount * 0.1);
box(50, 50)
pop();

```

Jeg har lavet boksen ved hjælp af p5 js. referencen box(). Først og fremmest ville jeg ikke have nogen fyldfarve på boksen, og har derfor brugt noFill(). Det gør, at det kun er linjerne "rundt" om boksen vi kan se. Jeg har valgt at linjerne, som danner boksen, skal være rødlige. Det har jeg gjort ved at bruge stroke(153, 0, 51). Farveneværdierne er RGB, dvs. (R, G, B) Hvor R = rød, G = grøn og B = blå. Ved at kombinere disse tre farver kan man lave en masse farver. Det føltes lidt som billedkunst i folkeskolen, hvor man lærte at rød og blå gav lilla. 

Fordi denne kode er skrevet i draw, betyder det at den vil køre igen og igen og igen. Så for at undgå at boksen vil køre oveni sig selv (jeg ved ikke hvordan jeg skal forklare det bedre...) har jeg indsat en baggrund her under draw og ikke i set-up.

For at få boksen til at rotere har jeg indsat en rotering omkring X aksen og en rotering omkring Y aksen: rotateX og rotateY. Jeg har angivet frameCount værdien efterfølgende.

Til sidst har jeg indsat koden box(w, h) hvor w er bredde og h er højden på boksen. Mine værdier var w = 50 og h = 50 for at skabe en boks hvor alle siderne var lige store.

Fordi jeg har mikset 3d og 2d (og bevægelse) har jeg indsat push() og pop() omkring hver "funktion". Push og pop skal altid bruges sammen. Som p5. js referencen beskriver, bruges de ofte til at få mere kontrol over sin kode. Push() funktionen gemmer det jeg har lavet under draw(). Og pop gendanner dem. Jeg har som sagt brugt disse, for at skabe mere kontrol i min kode. 

## Refleksioner om processen 
Ja, jeg har som sagt brugt push() og pop(). Jeg fandt ud af at det var smart at bruge disse funktioner, ved at gøre noget så simpelt: som at prøve sig frem. Uden push og pop begyndte alle mine former (dvs. firkatnerne, 3d boksen, cirklerne osv.) at bevæge sig og rotere i samme frameCount som 3d boksen. Ved at indsætte push og pop kunne jeg kontrollere at det kun var 3d boksen der skulle rotere om x og y aksen. Jeg vil gerne lære mere om hvordan man blander 3d med 2d, da jeg synes det skaber en spændende dynamik i programmet. 

Generelt har min tilgang til denne opgave været at lege mig lidt frem. Læse referencer på p5 js., og derefter prøve dem af. Det har hjulpet mig med, at få en eller anden form for forståelse for de funktioner jeg har valgt at bruge i min kode. Det er ligesom at bruge en ordbog når man lærer et nyt sprog. På den måde kan man godt sammenligne dét at kode med det at skrive.

Jeg har desuden haft meget sjovt med at lege med RGB farvekoder og alpha (gennemsigtigheden af farverne). Jeg synes, at det var spændende at finde ud af hvilke farveværdier der gav hvilke farver. Det var også fedt at lege med alpha, for at se hvordan mine farver ville se ud når de ligger ovenpå hinanden. Fx. når den ene firkant kører ovenpå den anden. Dér ændres farven.

Der er mange ting jeg stadig ikke forstår. Og mange ting jeg gerne vil prøve af. Men Mini X 1 har givet mig blod på tanden til at lære mere. Og indtil videre, synes jeg, at det har været sjovt at prøve mig frem. D

Specifikt gad jeg godt have en bedre forståelse for hvor x, y, z koordinanterne befinder sig på canvaset. Jeg prøvede mig frem indtil jeg ramte rigtigt. Det ville være fedt, hvis jeg bare vidste præcist hvilke værdier jeg skulle skrive, for at placere formerne. 

Det var det. Tak fordi du læste min ReadMe. :dancer:


#### Reference liste
[Hilma af Klint inspiration](https://plakaten.com/produkt-kategori/hilma-af-klint/)

[Richard Mortensen udstilling på Trapholt](https://trapholt.dk/richard-mortensen-salen/)


###### p5. js referencerne, jeg nævner i min ReadMe:
[WEBGL p5. js](https://p5js.org/reference/#/p5/WEBGL)

[push/pop p5. js](https://p5js.org/reference/#/p5/pop)

[box() p5. js](https://p5js.org/reference/#/p5/box)

[alpha p5. js](https://p5js.org/reference/#/p5/alpha)






function setup() {
  // put setup code here
  createCanvas(500, 500, WEBGL);
  //canvas, men ved WEBGL for at det kan være 3d
}

function draw() {
  // put drawing code here


 push();
noFill();
//ingen fyld
stroke(153, 0, 51);
//farve på linjerne, stroke
background(204, 153, 255);
//baggrund så den hele tiden indsætter en ny baggrund

rotateX(frameCount * 0.01);
rotateY(frameCount * 0.1);
box(50, 50)
pop();
//roterings punktet for

push();
fill(0, 100, 0, 10);
rotateZ(frameCount * 0.001);
ellipse(200, 200, 350);
pop();
//ellipse i højre hjørne

push();
fill(120, 0, 0, 100);
ellipse(-80, -300, 350);
pop();
//ellipse i venstre hjørne

noStroke();
fill(255, 153, 51);
  ellipse(0, 0, 20);
  
//tilføjet ellipse i centrum


noFill();
stroke(204, 255, 51);
ellipse(0, 0, 400);


//tilføjet ellipse uden fyld


noStroke();
fill(255, 204, 204, 90);
square(0, 0, 100);


//tilføjet en firkant uden stroke men med fyld med aplha

push();
noStroke();
fill(255, 153, 102, 150);
rotateZ(frameCount * 0.02);
square(0, 0, -100);
pop();


//tilføjet endnu en firkant, bare andre koordinater

}

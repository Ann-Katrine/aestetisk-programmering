# MiniX3 

**Link til mit program**

[Klik her for mit program](https://ann-katrine.gitlab.io/aestetisk-programmering/MiniX%203/index.html)

**Link til source koden**

[Klik her for source code](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX%203/miniX3.js)

**Screenshot af mit program**

![](screenshot3.png)

## Beskrivel af programmet
I denne uges minix skulle vi lave en throbber. Jeg har valgt at lave en computer med ansigtsudtryk. Den ser træt ud, og har en sveddråbe for "panden". Omkring computeren kører der et spørgsmålstegn i et cirkulært loop. Dette har til formål at afbillede den mere klassiske throbber, hvor et element kører rundt i et endeløst cirkulært loop. Hvis man klikker med musen vil computer-figuren lukke sine øjne. Det skal forestille at man her giver computeren en såkaldt "tænkepause". 

## Beskrivelse af koden
Det første jeg startede med at lave var min egen `function`, som jeg kunne kalde på i `function draw`. Jeg kalde funktionen for "computer", da funktionen bestod af alle de geometriske former som udgør min computer figur. Jeg vil ikke vise koden her, da den er meget simpel og består mest består af følgende syntakser: `ellipse()`, `rect()`, `quad()` og `triangle()`

Fordi jeg har valgt at min throbber også skulle ligne nogle af de throbbere vi kender fra andre medier, har jeg valgt at spørgsmålstegnet skulle bevæge sig i en cirkel rundt om computer-figuren. Det har jeg gjort således:

```
let vinkel = 0;
let speed = 5 + 5;

```

Først har jeg defineret to variabler: en vinkel- og en speed variabel.

Under funktionen draw har jeg kodet følgende:

```
push();
background(200, 204, 255, 10);
translate(width/2, height/2);

rotate(vinkel);
//roterer vinklen/variablen

textSize(50);
fill(0, 80);
textFont('Geogria')
text("?", 100, 200);

vinkel = vinkel + speed
pop();
//? roterer nu, og fungerer som en klassisk throbber

```

Jeg har brugt syntaksen `translate()`til at bestemme hvor midten af loopet skal være. 

Derefter har jeg indsat vinkel-variablen i `rotate()`. Og efterfølgende brugt syntaksen `text()` til at skrive mit spørgsmålstegn på canvaset. Jeg har valgt en font, en tekststørrelse og en farve. Så har jeg sat `vinkel = vinkel + speed`for at få spørgsmålstegnet til at bevæge sig rundt. Jeg brugt `push()` og `pop()`for at have bedre kontrol over hvilke elementer der ville bevæge sig. Sidst vil jeg nævne, at jeg i draw funktionen har tilføjet en gennemsigtig baggrund (fordi at draw funktionen kører i et loop) for at få det til at ligne at spørgsmålstegnet kører rundt og rundt. 

## Refleksion om temporalitet og min throbber
Jeg synes, at temporalitet var et svært emne, men interessant. Jeg fandt det især interessant hvordan vi opfatter tid. Mennesketid VS computertid. Jeg valgte at tage udgangspunkt i mit eget forhold til computertid og throbbere. Mens vi har haft dette emne, er jeg blevet gjort opmærksom på, at throbbere vækker en stor utålmodighed i mig. Jeg plejer at lukke fanen ned og starte forfra, hvis throbberen tager for lang tid. Og "lang tid" er faktisk ikke så lang tid igen... Det er gået op for mig, at det jo netop er her der sker en misforståelse mellem hvad mennesketid og computertid er. Og hvordan vi som mennesker kan have svært ved forestille os at computeren kører på en anden tid end os. Throbbere har jo til formål at være formidler af de usynlige processer der foregår indeni en computer - når vi, som mennesker, bare ser at computeren er "gået i stå".

Jeg har derfor gjort et forsøg på at bygge en bedre bro mellem mennesket og computeren, så vi forstår at computeren lige har brug for at gennemgå nogle processer. Jeg tænnkte "hvordan kan jeg gøre således, at jeg ikke selv bliver så utålmodig, når jeg ser en throbber?" Måske finde en måde hvorpå vi som mennesker kan relatere og sympatisere lidt med computeren? 

Derfor har jeg kodet en computer-figur med menneskelige træk. Jeg har med andre ord personificeret computeren. Den har trætte øjne, som kan lukkes, og en mund. Jeg har skrevet "jeg tænker lige - giv mig lidt tid" som om det var computeren der sagde det. Spørgsmålstegnene skal understrege at computeren er ved at finde ud af hvad den skal gøre. Det er meningen at det skal trække paralleller til når vi selv, som mennesker, har brug for en tænkepause. For vi kender jo alle sammen godt dét, når tingene går lidt for hurtigt og vi lige skal tænke lidt. Præcis ligesom når computeren har brug for tid til at gennemgå nogle usynlige processer. 

Jeg har kort fortalt: prøvet at formidle dét at computeren faktisk har brug for, at vi er lidt tålmodige når den viser en throbber.

Også spændende at overveje spørgsmålet om en computer er bevidst. Og ved at personificere min computer-figur, giver jeg en illusion af at computeren er selvbevidst omkring at den lige har brug for at tænke. 



